using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    [System.Serializable]
    private class AmmoSlot
    {
        public AmmoType ammoType;
        public int ammoCount;
    }

    [SerializeField]
    AmmoSlot[] ammoSlots;

    public int ReadAmmoCount(AmmoType ammoType)
    {
        return GetAmmoSlot(ammoType).ammoCount;
    }

    public void ReduceAmmoCount(AmmoType ammoType)
    {
        GetAmmoSlot(ammoType).ammoCount -= 1;
    }

    public void BoostAmmoCount(int boost, AmmoType ammoType)
    {
        GetAmmoSlot(ammoType).ammoCount += boost;
    }

    //To find which ammo slot, the ammo type is referring to.
    //Multiple ammo slots can have same ammo type.
    private AmmoSlot GetAmmoSlot(AmmoType ammoType)
    {
        foreach(AmmoSlot ammoSlot in ammoSlots)
        {
            if(ammoSlot.ammoType == ammoType)
            {
                return ammoSlot;
            }
        }

        return null;
    }
}
