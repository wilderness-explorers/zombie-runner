using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    [SerializeField]
    private float damage = 40.0f;

    private Transform target;
    private PlayerHealth playerHealth;

    void Start()
    {
        playerHealth = FindObjectOfType<PlayerHealth>();

        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    //Triggered from animation.
    private void EnemyAttackEvent()
    {
        if(target == null)
        {
            return;
        }

        Debug.Log("Bang bang");
        playerHealth.DecreaseHealth(damage);
    }
}
