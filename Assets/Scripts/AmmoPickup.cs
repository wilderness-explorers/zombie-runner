using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPickup : MonoBehaviour
{
    [Tooltip("Amount of ammo to be added for the player.")]
    [SerializeField]
    int ammoBoost;

    [SerializeField]
    AmmoType ammoType;

    Ammo ammo;

    private void Start()
    {
        ammo = FindObjectOfType<Ammo>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            ammo.BoostAmmoCount(ammoBoost, ammoType);
            Debug.Log("Pickup collected.");
            Destroy(this.gameObject);
        }
    }
}
