using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    
    
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        //Reseting the time scale that was set to 0, in player death handler.
        Time.timeScale = 1;
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
