using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeathHandler : MonoBehaviour
{
    [SerializeField]
    Canvas gameOverCanvas;

    void Start()
    {
        gameOverCanvas.enabled = false;
    }

    public void HandleDeath()
    {
        gameOverCanvas.enabled = true;

        //To avoid contradiction, when mouse pointer is used for UI after game is over.
        Time.timeScale = 0;

        //To avoid weapon control.
        FindObjectOfType<WeaponSwitcher>().enabled = false;

        //Change mouse pointer from game mode to cursor.
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
