using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class WeaponZoom : MonoBehaviour
{
    //Parameters
    [SerializeField]
    float zoomedInFOV = 20f, zoomedOutFOV = 60f;
    float zoomedInSensitivity = 0.2f, zoomedOutSensitivity = 2f;

    //Cache
    [SerializeField]
    Camera fpsCamera;
    
    [SerializeField]
    RigidbodyFirstPersonController fpsController;

    //State
    bool isZoomToggle;

    void Start()
    {
        isZoomToggle = false;
    }

    //To ensure the view is zoomed out, if player switches weapon.
    private void OnDisable()
    {
        ZoomOut();
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(1))
        {
            Debug.Log("Right mouse button clicked");

            if(isZoomToggle == false)
            {
                ZoomIn();
            }
            else
            {
                ZoomOut();
            }
        }
    }

    private void ZoomIn()
    {
        isZoomToggle = true;

        fpsCamera.fieldOfView = zoomedInFOV;
        fpsController.mouseLook.XSensitivity = zoomedInSensitivity;
        fpsController.mouseLook.YSensitivity = zoomedInSensitivity;
    }

    private void ZoomOut()
    {
        isZoomToggle = false;

        fpsCamera.fieldOfView = zoomedOutFOV;
        fpsController.mouseLook.XSensitivity = zoomedOutSensitivity;
        fpsController.mouseLook.YSensitivity = zoomedOutSensitivity;
    }
}
