using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDestination : MonoBehaviour
{
    [SerializeField]
    Canvas levelCompletedCanvas;

    void Start()
    {
        levelCompletedCanvas.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            levelCompletedCanvas.enabled = true;

            //To avoid contradiction, when mouse pointer is used for UI after game is over.
            Time.timeScale = 0;

            //To avoid weapon control.
            FindObjectOfType<WeaponSwitcher>().enabled = false;

            //Change mouse pointer from game mode to cursor.
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
