using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{
    private NavMeshAgent navMeshAgent;
    private float distanceToTarget = Mathf.Infinity;
    private bool isProvoked;

    private Transform target;
    [SerializeField]
    private float detectionRange = 5.0f;

    private float turnSpeed = 5.0f;

    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();

        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        //Distance between player and enemy.
        distanceToTarget = Vector3.Distance(target.position, transform.position);

        //Enemy starts attacking, once provoked (or) player is within range. Never stops till reaching near player.
        if (isProvoked)
        {
            //To stop enemy, if player runs out of detection range.
            isProvoked = false;
            
            EngageTarget();
        }
        else if (distanceToTarget < detectionRange)
        {
            isProvoked = true;
        }
        else
        {
            GetComponent<Animator>().SetBool("attack", false);
            GetComponent<Animator>().SetTrigger("idle");
        }
    }

    public void OnDamageTaken()
    {
        isProvoked = true;
    }

    private void EngageTarget()
    {
        FaceTarget();

        //Enemy stops moving completely, if its within stoppping distance to player.
        if (distanceToTarget > navMeshAgent.stoppingDistance)
        {
            ChaseTarget();
        }

        if(distanceToTarget <= navMeshAgent.stoppingDistance)
        {
            AttackTarget();
        }
    }

    private void ChaseTarget()
    {
        GetComponent<Animator>().SetTrigger("move");
        navMeshAgent.SetDestination(target.transform.position);
    }

    private void AttackTarget()
    {
        GetComponent<Animator>().SetBool("attack", true);
    }

    //Problem: When enemy is far away, the Nav mesh helps in rotating the enemy towards player. But in attack mode, the enemy does not look the player.
    //Solution:
    private void FaceTarget()
    {
        //Normalized indicates, we are interested in direction rather than magnitude. Returns vector with magnitude of 1.
        Vector3 direction = (target.position - transform.position).normalized;

        //0 for Y, as we dont want enemy to look up and down.
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));

        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, turnSpeed * Time.deltaTime);
    }

    //Listens for the event, when the object containing this class is selected.
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0, 0.75f);
        Gizmos.DrawWireSphere(transform.position, detectionRange);
    }
}