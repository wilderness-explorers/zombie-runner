using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashlightSystem : MonoBehaviour
{
    //Parameters
    [SerializeField]
    float intensityDecayRate = 0.05f;
    [SerializeField]
    float angleDecrementRate = 0.05f;
    [SerializeField]
    float minimumSpotAngle = 30;

    //Cache
    Light flashLight;

    void Start()
    {
        flashLight = GetComponent<Light>();
    }

    void Update()
    {
        DecayLightIntensity();
        DecreaseLightAngle();
    }

    private void DecayLightIntensity()
    {
        flashLight.intensity -= intensityDecayRate * Time.deltaTime;
    }

    private void DecreaseLightAngle()
    {
        if (flashLight.spotAngle >= minimumSpotAngle)
        {
            flashLight.spotAngle -= angleDecrementRate * Time.deltaTime;
        }
    }

    public void RestoreFlashLight(float intensity, float angle)
    {
        flashLight.intensity = intensity;
        flashLight.spotAngle = angle;
    }
}
