using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    //Parameters
    [SerializeField]
    private float initialHealth = 150.0f;
    private float playerHealth;

    //Cache
    PlayerDeathHandler playerDeathHandler;
    [SerializeField]
    Canvas bloodGraphicCanvas;
    [SerializeField]
    Transform bloodImage;

    void Start()
    {
        playerDeathHandler = GetComponent<PlayerDeathHandler>();

        playerHealth = initialHealth;

        bloodGraphicCanvas.enabled = false;
    }

    void Update()
    {
        
    }

    public void DecreaseHealth(float damage)
    {
        playerHealth -= damage;
        StartCoroutine(ShowGraphic());

        if(playerHealth <= 0)
        {
            playerDeathHandler.HandleDeath();
        }
    }

    private IEnumerator ShowGraphic()
    {
        bloodGraphicCanvas.enabled = true;
        bloodImage.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 120));
        yield return new WaitForSeconds(1.0f);
        bloodGraphicCanvas.enabled = false;
    }
}
