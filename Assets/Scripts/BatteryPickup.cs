using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryPickup : MonoBehaviour
{
    [SerializeField]
    float intensityRestoreValue = 4.0f;

    [SerializeField]
    float angleRestoreValue = 60.0f;
    
    void Start()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            other.GetComponentInChildren<FlashlightSystem>().RestoreFlashLight(intensityRestoreValue, angleRestoreValue);
            Destroy(this.gameObject);
        }
    }
}
