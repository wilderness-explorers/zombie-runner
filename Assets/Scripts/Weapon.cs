using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Weapon : MonoBehaviour
{
    [SerializeField]
    private Camera FPCamera;

    [SerializeField]
    private float range = 100f;

    [SerializeField]
    private float damage = 10.0f;

    [SerializeField]
    private ParticleSystem muzzleFlash;

    [SerializeField]
    private GameObject hitEffect;

    [SerializeField]
    private Ammo ammo;

    [Tooltip("To define what type of ammo this weapon will use.")]
    [SerializeField]
    private AmmoType ammoType;

    private bool canShoot;

    [SerializeField]
    private float gunReloadTime = 1.0f;

    [SerializeField]
    private TextMeshProUGUI ammoText;

    private void Awake()
    {
        FPCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    private void OnEnable()
    {
        canShoot = true;
    }

    void Update()
    {
        UpdateUI();

        //Mouse button.
        if (Input.GetButtonDown("Fire1") && (ammo.ReadAmmoCount(ammoType) > 0) && canShoot)
        {
            canShoot = false;
            StartCoroutine(ReloadWeapon());
            Shoot();
        }
    }

    private void UpdateUI()
    {
        ammoText.text = "Ammo: " + ammo.ReadAmmoCount(ammoType).ToString();
    }

    private void Shoot()
    {
        ammo.ReduceAmmoCount(ammoType);

        PlayMuzzleFlash();
        ProcessRaycast();
    }

    private IEnumerator ReloadWeapon()
    {
        yield return new WaitForSeconds(gunReloadTime);
        canShoot = true;
    }

    private void PlayMuzzleFlash()
    {
        muzzleFlash.Play();
    }

    private void ProcessRaycast()
    {
        RaycastHit hit;

        if (Physics.Raycast(FPCamera.transform.position, FPCamera.transform.forward, out hit, range))
        {
            //TODO: add some hit effect for visual players.
            CreateHitEffect(hit);

            EnemyHealth enemyHealth = hit.transform.GetComponent<EnemyHealth>();

            if (enemyHealth == null)
            {
                return;
            }

            //Call a method on enemy health that decreases enemy's health.
            enemyHealth.TakeDamage(damage);
        }
        else
        {
            //to avoid null reference, if player shoots sky for example.
            return;
        }
    }

    //VFX for bullet hit.
    private void CreateHitEffect(RaycastHit hit)
    {
        GameObject bulletHit = Instantiate(hitEffect, hit.transform.position, Quaternion.identity);

        Debug.Log("Hit effect");

        Destroy(bulletHit, 0.1f);
    }
}
