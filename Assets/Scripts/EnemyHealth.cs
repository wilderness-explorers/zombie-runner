using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField]
    private float hitPoints = 100f;

    //To reduce hit points, based on the amount of damage.
    public void TakeDamage(float damage)
    {
        BroadcastMessage("OnDamageTaken");

        hitPoints -= damage;

        if(hitPoints <= 0)
        {
            EnemyDeath();
        }
    }

    private void EnemyDeath()
    {
        GetComponent<Animator>().SetTrigger("death");
        GetComponent<EnemyAI>().enabled = false;
        GetComponent<EnemyAttack>().enabled = false;
        //Destroy(this.gameObject);
    }
}